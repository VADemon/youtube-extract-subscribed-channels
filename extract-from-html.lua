#!/usr/bin/env lua

local args = args or {...}
local filePath = args[1]
if not filePath then
	io.stderr:write("You must provide 1 argument: Path to file\n")
	os.exit(1)
end
local fileHandle, err = io.open(filePath, "r")
if not fileHandle then
	io.stderr:write("Error occured while reading file: ".. err .."\n")
	os.exit(2)
end

local formatter = require("formatter")

formatter:begin()
for line in fileHandle:lines() do
	-- <a id="endpoint" class="yt-simple-endpoint style-scope ytd-guide-entry-renderer" tabindex="-1" role="tablist" title="Fancy Channel Name" href="https://www.youtube.com/channel/UCcafebabe111100001337-_">
	local name, link = line:match([[title="([^"]+)" href="([^"]+)">]])
	--print(link, name)
	if name and link then
		if (link:find("/channel/") or link:find("/user/") or link:find("/c/")) and not link:find("studio%.youtube") then
			
			formatter:process(name, link)
			
		elseif link:find("feed/guide_builder") then
			-- end of subscription list reached
			break 			
		end
	end
end
formatter:finish()