# What is this tool for?

It allows you to create a list with channel names & links of your Youtube subscriptions, using one of two methods:

1. Extract from your Google Takout data (.csv): `subscriptions.csv`
2. Extract from HTML page of Youtube

In addition, you can customize the output format and exclude channels. I use it to output Shell scripts that archive using Youtube-dl.

## 1. Extract from Google Takeout .csv file

Steps:

1. Request a Youtube data Takeout. Pay attention to do it for the channel you want to have subscriptions of (Youtube allows you to manage different channels, Takeout for each channel is separate)

2. In the first archive, find `./Takeout/YouTube and YouTube Music/subscriptions/subscriptions.csv`

3. Copy the path to this file

4. Launch the script and save to file: `lua extract-from-takeoutcsv.lua /path/to/takeout/subscriptions.csv > channel_links.txt`

## 2. Extract subscribed channel links from Youtube homepage

Steps:

1. Open (Youtube)[https://youtube.com] (do NOT go to `https://www.youtube.com/feed/channels`!)
2. Make sure you see your "Subscriptions" channels on the left
3. Open the Inspector tab in browser's dev tools (where you see the HTML stuff):
    - On Firefox you can right-click anywhere on page and scroll to the very top
4. Right click on `<html ...` and then `Copy → Outer HTML`, open Notepad and save it to a file.
4. Now you need to start the script and point it to the saved `.html` file. For example:
   a. `lua extract-from-html.lua "Subscriptions - YouTube.htm"`
5. The script will extract every channel link it finds.
   a. If you want to save them to file, add ` > channel_links.txt` to the end of the command above

This was an alternative Step 3 that no longer works with Firefox, but it was the easiest:

> Save the current page as `.html` file (Right click anywhere on page → "Save page as...")

PS: If there're 0 extracted videos, the page layout probably changed and the script is broken :) Open a new issue then.

## Usage:

`lua extract-from-takeoutcsv.lua path/to/CSVFILE`

`lua extract-from-html.lua path/to/HTMLFILE`

The default output looks like this:

```
# RPCS3 - channel/UCz3-0QxNr4S4gK0xaWy7exQ
https://www.youtube.com/channel/UCz3-0QxNr4S4gK0xaWy7exQ

# Veritasium - channel/UCHnyfMqiRRG1u-2MsSQLbXA
https://www.youtube.com/channel/UCHnyfMqiRRG1u-2MsSQLbXA

# end of channel list

Channels extracted: 213
```

If you wish to change this format, edit the `.lua` at the lines 3-20 (or create a new file `custom-format-variables.lua` redefining those variables)

The script will try to match any of the channel link formats: `/user/NAME`, `/c/SHORT_NAME`, `/channel/UC_YoutubeChannel_ID`

If you wish to exclude any channels from the output, see `excluded-channels-list.txt` where you can enter Channel IDs to blacklist them.

The links are output in the order in which they appear in the .html file.

## Installation / requirements:

Lua.

For Debian/Ubuntu: `sudo apt-get install lua`

For Archlinux/Manjaro: `sudo pacman -Sy lua`

Windows: Lua For Windows / cygwin (package: lua) / any other installation method

## See also

Another method using Google Takeout (as well as others in the comments): https://www.reddit.com/r/DataHoarder/comments/jkqfp8/youtube_subscription_manager/

A Mozilla Addon from the above thread, exports in "the XML format": https://github.com/ajohntom/tube-sub-export

An automatic subscription downloader: https://www.reddit.com/r/DataHoarder/comments/bg1g78/i_made_a_youtubedl_script_to_download_all_youtube/
