#!/usr/bin/env lua

local args = args or {...}
local filePath = args[1]
if not filePath then
	io.stderr:write("You must provide 1 argument: Path to file\n")
	os.exit(1)
end
local fileHandle, err = io.open(filePath, "r")
if not fileHandle then
	io.stderr:write("Error occured while reading file: ".. err .."\n")
	os.exit(2)
end

local formatter = require("formatter")

local firstLineLower = fileHandle:read("*l"):lower()
-- determine CSV columns for values
local columnChannelId
local columnUrl
local columnName

local columnCount = 0
for cell in firstLineLower:gmatch("[^,]+") do
	-- cell already contains lower case text
	columnCount = columnCount + 1
	if cell:find("channel id", 1, true) then
		columnChannelId = columnCount
	elseif cell:find("channel url", 1, true) then
		columnUrl = columnCount
	elseif cell:find("channel title", 1, true) then
		columnName = columnCount
	else
		io.stderr:write("Warning, unknown column: ".. cell .."\n")
	end
end

if not columnChannelId or not columnUrl or not columnName then
	io.stderr:write("Could not auto-detect all columns in .csv:\n")
	io.stderr:write("columnChannelId = ".. tostring(columnChannelId) .."\n")
	io.stderr:write("columnUrl = ".. tostring(columnUrl) .."\n")
	io.stderr:write("columnName = ".. tostring(columnName) .."\n")
	os.exit(3)
end

formatter:begin()

for line in fileHandle:lines() do
	local columnCount = 0
	local name, link
	for cell in line:gmatch("[^,]+") do
		columnCount = columnCount + 1
		-- count columns and fill in variable values
		if columnCount == columnUrl then
			link = cell
		elseif columnCount == columnName then
			name = cell
		end
		-- got needed columns?
		if name and link then
			formatter:process(name, link)
			break
		end		
	end
end
formatter:finish()