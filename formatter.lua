#!/usr/bin/env lua

local formatter = {
	channelList = {},
	count = 0,
	httpToHttps = true,
	-- loaded from file:
	outputPrefix = "# start of channel list",
	outputSuffix = "# end of channel list",
	outputFormat = [[
# %name% - %handle%
%link%"]]
}

do
	-- you can overwrite the formatting variables here if you edit that file
	local customFormatFileName = "custom-format-variables.lua"
	local file, err = io.open(customFormatFileName, "r")
	if file then
		-- file exists, load
		file:close()
		local f = formatter
		-- load returned values from file
		f.outputPrefix, f.outputSuffix, f.outputFormat = dofile(customFormatFileName)
	else
		-- file doesn't exist, try to save a preset
		local file, err = io.open(customFormatFileName, "w")
		if file then
			file:write([=====[--[[
Edit these to your heart's content
]]
local outputPrefix = [===[
# start of channel list
]===]

local outputSuffix = [===[
# end of channel list
]===]

-- WRITE YOUR CUSTOM OUTPUT FORMAT HERE
-- e.g.:   ./dl_channel.sh %link%
local outputFormat = [===[
# %name% - %handle%
%link%
]===]

return outputPrefix, outputSuffix, outputFormat]=====])
		else
			io.stderr:write("Warning: could not auto-create a default preset file!\n")
		end
	end
end


function formatter:extractChannelHandle(link)
	local id = 
		link:match("channel/UC[^\"/]+")
		or link:match("user/[^\"/]+")
		or link:match("c/[^\"/]+")
		
	return id
end
function formatter:printChannel(name, link)
	if self.httpToHttps then
		link = link:gsub("[hH][tT][tT][pP]://", "https://")
	end
	local replace = {
		["name"] = name,
		["link"] = link,
		["handle"] = self:extractChannelHandle(link)
	}

	print(( self.outputFormat:gsub("%%([A-Za-z0-9_-]+)%%", replace) ))
end

function formatter:loadExcludedChannels()
	local patterns = {}
	
	local fileName = "excluded-channels-list.txt"
	local file, err = io.open(fileName, "r")
	if not file then
		io.stderr:write("INFO: Exclude list was not loaded: ".. tostring(err) .. "\n")
		return {}
	end
	
	for line in file:lines() do
		if not line:find("^#") then
			local trimmed = line:match("^ *(.-) *$")
			if #trimmed > 0 then
				table.insert(patterns, trimmed)
				io.stderr:write("Add exclude pattern: ".. trimmed .."\n")
			end
		end
	end
	
	return patterns
end

function formatter:isExcluded(patterns, link)
	for i = 1, #patterns do
		if link:find(patterns[i], 1, true) then
			return true, patterns[i]
		end
	end
	return false
end

function formatter:begin(prefix)
	if prefix == nil then
		prefix = formatter.outputPrefix
	end
	if #prefix > 0 then
		print(prefix)
	end
end

function formatter:process(name, link)
	if not self.channelList[link] then
		local excluded, excludePattern = self:isExcluded(self.excludePatterns, link)
		
		if not excluded then
			self.channelList[link] = name
			self.count = self.count+1
			self:printChannel(name, link)
		else
			io.stderr:write(string.format("Channel '%s' at '%s' excluded by pattern '%s'\n", name, link, excludePattern))
		end
	end
end

function formatter:finish(suffix)
	if suffix == nil then
		suffix = formatter.outputSuffix
	end
	if #suffix > 0 then
		print(suffix)
	end
	io.stderr:write("Channels extracted: ".. formatter.count)
end

formatter.excludePatterns = formatter:loadExcludedChannels()
return formatter